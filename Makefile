SHELL = bash

.PHONY: publish
publish:
	if [[ ! -z $$(git status -s) ]]; then \
		git add -A && git commit -m "update index" && git push origin main; \
	fi

.PHONY: render # -----------------------------------------------------
render: public/index.html README.md

public/index.html: index.Rmd
	Rscript -e "rmarkdown::render(\"index.Rmd\", output_file=\"public/index.html\")"

README.md: index.Rmd
	cp $< $@
