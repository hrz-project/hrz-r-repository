---
title: "Horizon package repository"
author: s. barreto
date: "2021-03-23"
output:
  distill::distill_article:
    code_folding: false
    toc: true
    theme: theme.css
---

# Description

This is the R package repository for the horizon project.
You can install individual package from this repository using

```{R, eval=FALSE}
install.packages("biocblast", repos="https://hrz-project.gitlab.io/hrz-r-repository/")
```

If you plan on using these packages frequently, you can add this repository to the list of repository `install.packages` will check on by editing your `~/.Rprofile` file:

```{R, eval=FALSE}
options(repos=c("https://hrz-project.gitlab.io/hrz-r-repository/", getOption("repos"))
```

Consult the [PACKAGES](/src/contrib/PACKAGES) file for a list of
available packages.

# Package list

- **biocblast**:
- **krrraken**
- **blastClust**

<!-- Local Variables: -->
<!-- mode: markdown -->
<!-- End: -->
